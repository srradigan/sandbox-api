from fastapi import APIRouter
from uuid import UUID, uuid4
from fastapi import HTTPException, status, Response
from pydantic import BaseModel
from fastapi.encoders import jsonable_encoder
from enum import Enum
from typing import List
from pymongo.errors import PyMongoError
import asyncio
from asyncio import sleep
from ..persistence import db, config
from ..proxmox import proxmox

router = APIRouter(prefix="/jobs", tags=["Jobs"])

collection = db.jobs


class ActionsEnum(str, Enum):
    deleting = "DELETING"


class JobBase(BaseModel):
    action: ActionsEnum
    resource: UUID

    class Config:
        schema_extra = {"example": {"description": "An Example Job"}}


class JobIn(JobBase):
    pass


class JobOut(JobBase):
    id: UUID

    class Config:
        schema_extra = {"example": {"id": uuid4(), "description": "An Example Job"}}


async def fetch_jobs(job_id: UUID = None, auto_error: bool = True) -> List[JobOut]:
    query = {}

    if job_id:
        query["id"] = job_id

    try:
        result = [job async for job in collection.find(query)]
        if auto_error and len(result) == 0:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Job not found")
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to read DB"
        )

    return result


@router.get("", response_model=List[JobOut])
async def read_jobs():
    return await fetch_jobs(auto_error=False)


async def create_job(job: JobIn):
    try:
        result = await collection.insert_one({"id": uuid4(), **jsonable_encoder(job)})
        return await collection.find_one({"_id": result.inserted_id})
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to create DB entry"
        )


@router.get("/{job_id}", response_model=JobOut)
async def read_job(job_id: UUID):
    return (await fetch_jobs(job_id=job_id))[0]


@router.delete("/{job_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_job(job_id: UUID):
    await fetch_jobs(job_id=job_id)

    try:
        await collection.delete_one({"id": job_id})
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to delete DB entry"
        )

    return Response(status_code=status.HTTP_204_NO_CONTENT)


async def remove_environment(environment_id: UUID):
    env = await db.environments.find_one({"id": UUID(environment_id)})
    vms = {vm["vmid"]: vm for vm in proxmox.pools[config['proxmox']['pool']]}
    for vmid in env["vmids"]:
        if vmid in vms.keys():
            target = vms[vmid]

            acl = proxmox.get("/access/acl")

            for rule in acl:
                if rule["path"] == f"/vms/{vmid}":
                    proxmox.put(
                        "/access/acl",
                        {
                            "path": f"/vms/{vmid}",
                            "roles": rule["roleid"],
                            "users": [rule["ugid"]],
                            "delete": True,
                        },
                    )

            proxmox.post(f"/nodes/{target['node']}/qemu/{vmid}/status/stop", payload={})

            proxmox.delete(f"/nodes/{target['node']}/qemu/{vmid}")
        else:
            await db.environments.update_one(
                {"id": UUID(environment_id)}, {"$pull": {"vmids": vmid}}
            )

    if not len(env["vmids"]):
        try:
            await db.environments.delete_one({"id": UUID(environment_id)})
            return True
        except PyMongoError:
            raise HTTPException(
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Failed to delete DB entry",
            )
    return False


async def process():
    while True:
        for job in await read_jobs():
            if job["action"] == ActionsEnum.deleting:
                done = await remove_environment(job["resource"])
                if done:
                    await delete_job(job["id"])

        await asyncio.sleep(2)


loop = asyncio.get_running_loop()
loop.create_task(process())
