import toml
from motor.motor_asyncio import AsyncIOMotorClient

config = toml.load("config.toml")

mongo_client = AsyncIOMotorClient(config.get("mongodb").get("url"))

db = mongo_client.sandbox
