from fastapi import FastAPI, Depends
from .routers import templates, bundles, environments, jobs
from .security import AdminCheck, EitherCheck

app = FastAPI(title="Sandbox API", version="0.9.0")
app.include_router(templates.router, dependencies=[Depends(AdminCheck)])
app.include_router(bundles.router, dependencies=[Depends(AdminCheck)])
app.include_router(environments.router, dependencies=[Depends(EitherCheck)])
app.include_router(jobs.router, dependencies=[Depends(AdminCheck)])
