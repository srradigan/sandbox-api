from os import stat
import requests
from .persistence import config
from urllib3.exceptions import InsecureRequestWarning
from fastapi import HTTPException, status

requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)


class ProxmoxSession(requests.Session):
    def __init__(self, base_url=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.base_url = base_url

    def request(self, method, url, *args, **kwargs):
        url = self.base_url + url
        return super().request(method, url, *args, **kwargs)


class Proxmox:
    def __init__(self, url, ssl=True, token=None):
        self.url = url
        self.ssl = ssl
        self.token = token
        self.session = ProxmoxSession(base_url=f"{url}/api2/json")
        self.session.headers.update({"Authorization": f"PVEAPIToken={token}"})
        self.session.verify = ssl

    def get(self, url: str):
        try:
            result = self.session.get(url)
            result.raise_for_status()
            return result.json()["data"]
        except requests.HTTPError:
            raise HTTPException(status_code=result.status_code)

    def post(self, url: str, payload: dict):
        try:
            result = self.session.post(url, json=payload)
            result.raise_for_status()
            return result.json()["data"]
        except requests.HTTPError:
            raise HTTPException(status_code=result.status_code)

    def put(self, url: str, payload: dict):
        try:
            result = self.session.put(url, json=payload)
            result.raise_for_status()
            return result.json()["data"]
        except requests.HTTPError:
            raise HTTPException(status_code=result.status_code)

    def delete(self, url: str):
        try:
            result = self.session.delete(url)
            result.raise_for_status()
        except requests.HTTPError:
            raise HTTPException(status_code=result.status_code)

    @property
    def nodes(self):
        return self.get("/nodes")

    @property
    def users(self):
        return self.get("/access/users")

    @property
    def pools(self):
        result = {}
        for pool in self.get("/pools"):
            pool_id = pool["poolid"]
            result[pool_id] = self.get(f"/pools/{pool_id}")["members"]
        return result


proxmox = Proxmox(
    config["proxmox"]["url"],
    ssl=config["proxmox"]["ssl_verify"],
    token=config["proxmox"]["api_token"],
)
