from fastapi import APIRouter
from uuid import UUID, uuid4
from fastapi import HTTPException, status, Depends, Response
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from enum import Enum
from typing import List, Optional, FrozenSet
from pymongo.errors import PyMongoError
from random import randint
from ..persistence import db, config
from ..security import Auth, EitherCheck
from ..proxmox import proxmox
from .bundles import read_bundle
from .jobs import create_job, JobIn
from .templates import read_template
from asyncio import sleep
import re

router = APIRouter(prefix="/environments", tags=["Environments"])

collection = db.environments


class StateEnum(str, Enum):
    running = "RUNNING"
    stopped = "STOPPED"
    creating = "CREATING"
    deleting = "DELETING"
    error = "ERROR"


class EnvironmentBase(BaseModel):
    owner: str
    bundle: UUID

    class Config:
        schema_extra = {
            "example": {
                "owner": "someone@example.com",
                "bundle": uuid4(),
                "state": "RUNNING",
                "vmids": [100, 200],
            }
        }


class EnvironmentIn(EnvironmentBase):
    class Config:
        schema_extra = {"example": {"owner": "someone@example.com", "bundle": uuid4()}}


class EnvironmentOut(EnvironmentBase):
    id: UUID
    state: StateEnum
    vmids: FrozenSet[int]

    class Config:
        schema_extra = {
            "example": {
                "id": uuid4(),
                "owner": "someone@example.com",
                "bundle": uuid4(),
                "state": "RUNNING",
                "vmids": [100, 200],
            }
        }


class EnvironmentUpdate(EnvironmentBase):
    state: Optional[str]
    vmids: Optional[FrozenSet[int]]
    owner: Optional[str]
    bundle: Optional[str]


async def fetch_environments(
    auto_error: bool = True,
    environment_id: UUID = None,
    bundle_id: UUID = None,
    show_deleting: bool = False,
    auth: Auth = None,
) -> List[EnvironmentOut]:
    query = {}

    if environment_id:
        query["id"] = environment_id
    if bundle_id:
        query["bundle_id"] = bundle_id

    if "Administrator" not in auth.roles:
        query["owner"] = auth.username

    if not show_deleting:
        query["state"] = {"$not": {"$eq": StateEnum.deleting}}

    try:
        result = [environment async for environment in collection.find(query)]
        if auto_error and len(result) == 0:
            raise HTTPException(
                status.HTTP_404_NOT_FOUND, detail="Environment not found"
            )
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to read DB"
        )

    return result


async def validate_bundle(bundle_id):
    try:
        return await read_bundle(bundle_id)
    except HTTPException as e:
        if e.status_code == 404:
            raise HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=f"Invalid bundle: {bundle_id}",
            )
        else:
            raise e


async def create_user(username: str):
    auth_realm = config["proxmox"]["auth_realm"]
    user = f"{username}@{auth_realm}"
    existing = proxmox.get(f"/access/users/{user}")
    if not existing:
        proxmox.post("/access/users", payload={"userid": user})


async def new_vlan():
    while await collection.find_one({"vlan": (vlan := randint(1, 4000))}):
        pass
    return vlan


async def new_vmid():
    id_range = config["proxmox"]["id_ranges"][0]
    while await collection.find_one(
        {"vmids": {"$in": [(vmid := randint(id_range["start"], id_range["end"]))]}}
    ):
        pass
    return vmid


async def clone_vm(t, new_id):
    proxmox.post(
        f"/nodes/{config['proxmox']['deploy_node']}/qemu/{t['vmid']}/clone",
        payload={
            "newid": new_id,
            "pool": config["proxmox"]["pool"],
            "description": " ",
            "name": t["name"],
        },
    )


async def set_perms(association, user, new_id):
    full_user = f"{user}@{config['proxmox']['auth_realm']}"
    proxmox.put(
        "/access/acl",
        {
            "path": f"/vms/{new_id}",
            "roles": association["user_role"],
            "users": [full_user],
        },
    )


async def update_network(new_id, vlan):
    res = proxmox.get(f"/nodes/{config['proxmox']['deploy_node']}/qemu/{new_id}/config")
    net_dev = [n for n in res.keys() if n.startswith("net")]
    if not len(net_dev):
        return
    updated = {}
    for dev in net_dev:
        updated[dev] = re.sub(
            "bridge=\w+", f"bridge={config['proxmox']['bridge'][0]['name']}", res[dev]
        )
        updated[dev] = re.sub("tag=\d+", f"tag={vlan}", updated[dev])
    proxmox.post(
        f"/nodes/{config['proxmox']['deploy_node']}/qemu/{new_id}/config",
        payload=updated,
    )


@router.post("", response_model=EnvironmentOut, status_code=status.HTTP_201_CREATED)
async def create_environment(environment: EnvironmentIn, auth=Depends(EitherCheck)):
    if "Administrator" not in auth.roles:
        await fetch_environments(bundle=environment.bundle, auth=auth)

    bundle = await validate_bundle(environment.bundle)

    await create_user(auth.username)

    vlan = await new_vlan()
    vmids = []

    for association in bundle["templates"]:
        t = await read_template(UUID(association["template_id"]))
        new_id = await new_vmid()
        vmids.append(new_id)
        await clone_vm(t, new_id)
        await sleep(1)
        await set_perms(association, auth.username, new_id)
        await update_network(new_id, vlan)

    try:
        result = await collection.insert_one(
            {
                "id": uuid4(),
                "state": "STOPPED",
                "vmids": vmids,
                "vlan": vlan,
                **jsonable_encoder(environment),
            }
        )
        return await collection.find_one({"_id": result.inserted_id})
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to create DB entry"
        )


@router.get("", response_model=List[EnvironmentOut])
async def read_environments(auth=Depends(EitherCheck), bundle: UUID = None):
    return await fetch_environments(auto_error=False, auth=auth, bundle_id=bundle)


@router.get("/{environment_id}", response_model=EnvironmentOut)
async def read_environment(environment_id: UUID, auth=Depends(EitherCheck)):
    return (await fetch_environments(environment_id=environment_id, auth=auth))[0]


async def update_environment(
    environment_id: UUID, environment: EnvironmentUpdate, auth=Depends(EitherCheck)
):
    await fetch_environments(environment_id=environment_id, auth=auth)

    try:
        return await collection.find_one_and_update(
            {"id": environment_id},
            {"$set": jsonable_encoder(environment.dict(exclude_unset=True))},
            return_document=True,
        )
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to update DB entry"
        )


@router.delete("/{environment_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_environment(environment_id: UUID, auth=Depends(EitherCheck)):
    await update_environment(
        environment_id=environment_id,
        environment=EnvironmentUpdate(state="DELETING"),
        auth=auth,
    )

    await create_job(JobIn(action="DELETING", resource=environment_id))

    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.put("/{environment_id}/start", status_code=status.HTTP_204_NO_CONTENT)
async def start_enviroment(environment_id: UUID, auth=Depends(EitherCheck)):
    environment = (await fetch_environments(environment_id=environment_id, auth=auth))[
        0
    ]

    for vmid in environment["vmids"]:
        node = config['proxmox']['deploy_node']
        proxmox.post(f"/nodes/{node}/qemu/{vmid}/status/start", payload={})

    await update_environment(
        environment_id=environment_id, environment=EnvironmentUpdate(state="RUNNING")
    )
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.put("/{environment_id}/stop", status_code=status.HTTP_204_NO_CONTENT)
async def stop_enviroment(environment_id: UUID, auth=Depends(EitherCheck)):
    environment = (await fetch_environments(environment_id=environment_id, auth=auth))[
        0
    ]

    for vmid in environment["vmids"]:
        node = config['proxmox']['deploy_node']
        proxmox.post(
            f"/nodes/{node}/qemu/{vmid}/status/shutdown", payload={"forceStop": 1}
        )

    await update_environment(
        environment_id=environment_id, environment=EnvironmentUpdate(state="STOPPED")
    )
    return Response(status_code=status.HTTP_204_NO_CONTENT)
