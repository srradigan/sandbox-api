from fastapi import HTTPException, status, Depends
from starlette.requests import Request
from fastapi.security import APIKeyHeader, APIKeyCookie
from pydantic import BaseModel, Field
from typing import List
import requests
import re
from urllib.parse import unquote_plus
from sandbox.persistence import db, config

collection = db.users


class Auth(BaseModel):
    username: str
    roles: List[str]
    type: str

    class Config:
        schema_extra = {
            "example": {
                "username": "sandbox@ncaecybergames.org",
                "role": ["Administrator"],
                "type": "APIKey",
            }
        }


class APIKey(APIKeyHeader):
    async def __call__(self, request: Request) -> Auth:
        try:
            api_key: str = await super().__call__(request=request)
        except HTTPException:
            api_key = None
        if not api_key:
            if self.auto_error:
                raise HTTPException(
                    status.HTTP_401_UNAUTHORIZED, detail="No API Key Provided"
                )
            return "MISSING"

        result = await collection.find_one({"key": api_key})
        if not result:
            if self.auto_error:
                raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Access Denied")
            return 'DENIED'

        return Auth(
            username=result["username"],
            roles=result["roles"],
            type="X-API-KEY",
        )


class WPCookie(APIKeyCookie):
    async def __call__(self, request: Request) -> Auth:
        r = re.compile(self.model.name + ".*")
        matches = list(filter(r.match, request.cookies.keys()))

        if not len(matches):
            if self.auto_error:
                raise HTTPException(status.HTTP_401_UNAUTHORIZED)
            return "MISSING"

        value = unquote_plus(request.cookies.get(matches[0]))

        res = requests.head(
            f"{config['wordpress']['url']}/wp-admin/profile.php",
            cookies={matches[0]: value},
        )
        if res.status_code != 200:
            if self.auto_error:
                raise HTTPException(status.HTTP_403_FORBIDDEN)
            return "DENIED"

        return Auth(
            username=value.split("|")[0],
            roles=["User"],
            type="Wordpress",
        )


cookie_scheme = WPCookie(name="wordpress_sec")
manual_cookie = WPCookie(name="wordpress_sec", auto_error=False)

header_scheme = APIKey(name="X-API-KEY")
manual_header = APIKey(name="X-API-KEY", auto_error=False)

async def EitherCheck(cookie: Auth = Depends(manual_cookie), key: Auth = Depends(manual_header)) -> Auth:
    methods = [cookie, key]
    if any([method == 'DENIED' for method in methods]):
        raise HTTPException(status.HTTP_403_FORBIDDEN)
    if all([method == 'MISSING' for method in methods]):
        raise HTTPException(status.HTTP_401_UNAUTHORIZED)
    for method in methods:
        if isinstance(method, Auth):
            return method
    raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR)

async def AdminCheck(auth: Auth = Depends(header_scheme)) -> Auth:
    if "Administrator" not in auth.roles:
        raise HTTPException(status.HTTP_403_FORBIDDEN)
    return auth
