from fastapi import APIRouter
from uuid import UUID, uuid4
from fastapi import HTTPException, status, Depends, Response
from pydantic import BaseModel
from typing import List, Optional
from pymongo.errors import PyMongoError
from fastapi.encoders import jsonable_encoder
from ..persistence import db, config
from ..security import Auth, AdminCheck
from ..proxmox import proxmox

router = APIRouter(prefix="/templates", tags=["Templates"])

collection = db.templates


class TemplateBase(BaseModel):
    name: str
    vmid: int
    description: Optional[str]

    class Config:
        schema_extra = {
            "example": {
                "name": "Example",
                "description": "An Example Template",
                "vmid": 1000,
            }
        }


class TemplateIn(TemplateBase):
    pass


class TemplateOut(TemplateBase):
    id: UUID

    class Config:
        schema_extra = {
            "example": {
                "id": uuid4(),
                "name": "Example",
                "description": "An Example Template",
                "vmid": 1000,
            }
        }


class TemplateUpdate(TemplateBase):
    name: Optional[str]
    vmid: Optional[str]
    description: Optional[str]


async def fetch_templates(
    template_id: UUID = None,
    auto_error: bool = True,
    name: str = None,
    vmid: int = None,
) -> List[TemplateOut]:
    query = {}

    if template_id:
        query["id"] = template_id
    if name:
        query["name"] = name
    if vmid:
        query["vmid"] = vmid

    try:
        result = [template async for template in collection.find(query)]
        if auto_error and len(result) == 0:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Template not found")
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to read DB"
        )

    return result


async def validate_template(vmid):
    if not any(
        vm["vmid"] == vmid
        for vm in proxmox.pools[config["proxmox"]["pool"]]
        if vm["template"]
    ):
        raise HTTPException(
            status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"VMID {vmid} not found in Pool",
        )


@router.post("", response_model=TemplateOut, status_code=status.HTTP_201_CREATED)
async def create_template(template: TemplateIn, auth: Auth = Depends(AdminCheck)):
    await validate_template(template.vmid)

    try:
        result = await collection.insert_one(
            {"id": uuid4(), "creator": auth.username, **jsonable_encoder(template)}
        )
        return await collection.find_one({"_id": result.inserted_id})
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to create DB entry"
        )


@router.get("", response_model=List[TemplateOut])
async def read_templates(name: str = None, vmid: int = None):
    return await fetch_templates(auto_error=False, name=name, vmid=vmid)


@router.get("/{template_id}", response_model=TemplateOut)
async def read_template(template_id: UUID):
    return (await fetch_templates(template_id=template_id))[0]


@router.patch("/{template_id}", response_model=TemplateOut)
async def update_template(template_id: UUID, template: TemplateUpdate):
    await fetch_templates(template_id=template_id)

    if template.vmid:
        await validate_template(template.vmid)

    try:
        return await collection.find_one_and_update(
            {"id": template_id},
            {"$set": jsonable_encoder(template.dict(exclude_unset=True))},
            return_document=True,
        )
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to update DB entry"
        )


@router.delete("/{template_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_template(template_id: UUID):
    await fetch_templates(template_id=template_id)

    try:
        await collection.delete_one({"id": template_id})
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to delete DB entry"
        )

    return Response(status_code=status.HTTP_204_NO_CONTENT)
